#!/bin/bash
set -eux

if [ -z ${1+x} ] || [ -z ${2+x} ] || [ -z ${3+x} ]; then
  echo "ERROR: three parameters required: repository, docker compose location and branch"
  exit 1
fi

repository=$1
dockerCompose=$2
branch=$3
sharedPath=$4

if [ "${branch}" = "latest" ]; then
  branch=$(curl -s "${repository}/raw/master/version.txt")
fi

curl -s "${repository}/raw/${branch}/${dockerCompose}" >docker-compose.yaml

if [ ! -f "docker-compose.yaml" ]; then
  echo "ERROR: docker-compose.yaml file not found"
  exit 1
fi

docker rm -f "$(cat -- docker-compose.yaml | sed -nr 's/container_name: (.*)/\1/p' | xargs)" || true

echo BRANCH="${branch}" > .env
echo SHARED_PATH="${sharedPath}" >> .env
docker-compose -f docker-compose.yaml --project-directory . pull
docker-compose -f docker-compose.yaml --project-directory . up -d --remove-orphans
