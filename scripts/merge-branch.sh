#!/bin/bash
set -eux

if [ -z ${1+x} ] || [ -z ${2+x} ] || [ -z ${3+x} ]; then
  echo "ERROR: three parameters required: repository, origin and destination"
  exit 1
fi

repository=$1
origin=$2
dest=$3

if [ -d "repo" ]; then
  cd repo || exit 1
  git checkout "${dest}"
  git reset --hard HEAD~10 # revert some commits to prevent possible conflicts
  git fetch
  git pull
else
  git clone "${repository}" repo
  cd repo || exit 1
  git checkout "${dest}"
fi

git merge origin/"${origin}"
git push

cd ..
