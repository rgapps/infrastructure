#!/bin/bash
set -eux

chmod 400 /etc/ssh/ssh_host_rsa_key

/usr/sbin/sshd -D -e
