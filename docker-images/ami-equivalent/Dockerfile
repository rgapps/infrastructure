FROM fedora:33

ENV DOCKER_VERSION 19.03.5

SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
# hadolint ignore=DL3013,DL3033
RUN yum install -y ca-certificates curl python unzip \
# ssh
    openssh-server openssh-clients && \
    mkdir /var/run/sshd && \
# docker: https://stackoverflow.com/questions/44803773/install-docker-using-a-dockerfile
    curl -fsSL "https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz" \
    | tar -xzC /usr/local/bin --strip=1 docker/docker && \
# python and pip for ansible
    yum install -y python-pip rsync && \
    rsync --daemon && \
    pip install --no-cache-dir docker && \
    yum clean all && \
# permissions
    chmod 777 /run && \
    chmod 777 /usr/local && \
    chmod 777 /usr/local/lib && \
    chmod 777 /var/cache && \
    chmod 777 /usr/lib && \
    chmod 777 /usr/bin && \
    chmod 777 /usr/bin/python && \
    chmod 777 /usr/bin/yum && \
# add  user
    adduser ec2-user && \
    usermod -aG root ec2-user && \
    echo "ec2-user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

COPY entrypoint.sh /
RUN chmod 777 /entrypoint.sh

USER ec2-user


ENTRYPOINT ["/entrypoint.sh"]
