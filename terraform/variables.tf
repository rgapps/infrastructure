// TODO: See trello: https://trello.com/c/GNS4KfHo/40-1-improve-deploy-locally-using-terraform
variable "deploy_dev" {
  type = bool
  default = false
  description = "true to deploy dev environment"
}
variable "aws-ami-id" {
  type = string
  default = "ami-00eb20669e0990cb4"
}
variable "aws-access-key" {
  type = string
  default = ""
  description = "credentials of aws. Don't commit to repo"
}
variable "aws-secret-key" {
  type = string
  default = ""
  description = "credentials of aws. Don't commit to repo"
}
variable "ssh-public-key" {
  default = ""
  description = "location of key to connect with SSH"
}
variable "ssh-key-private" {
  default = ""
  description = "location of key to connect with SSH"
}
variable "godaddy-key" {
  type = string
  default = ""
}
variable "godaddy-secret" {
  type = string
  default = ""
}
variable "remote-public-ip" {
  type = string
  default = "127.0.0.1"
}
