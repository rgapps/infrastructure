resource "aws_key_pair" "aws-key" {
  count       = 1
  key_name   = "aws-key"
  public_key = file(var.ssh-public-key)
}

resource "aws_security_group" "ssh" {
  count       = 1
  name        = "ssh"
  description = "Security group for nat instances that allows SSH"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${data.http.local-public-ip.body}/32",
      "${var.remote-public-ip}/32"
    ]
  }
}
resource "aws_security_group" "internet-access" {
  count       = 1
  name        = "internet-access"
  description = "enables VPN traffic from internet"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
}
resource "aws_security_group" "production-page" {
  count       = 1
  name        = "production-page"
  description = "enables access to pages"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
}
resource "aws_security_group" "gocd-server" {
  count       = var.deploy_dev == true ? 1 : 0
  name        = "gocd-server"
  description = "enables access to gocd-server"

  ingress {
    from_port   = 8153
    to_port     = 8154
    protocol    = "tcp"
    cidr_blocks = [
      "${data.http.local-public-ip.body}/32",
      "${var.remote-public-ip}/32",
      "${aws_instance.production-instance.0.public_ip}/32"
    ]
  }
}
