# RGApps continuous delivery

The project includes all the related scripts required to deploy my infrastructure from scratch

# Pre-requisites

- gpg
- git-crypt
- terraform
- ansible

# Structure

 - **Variables.tf:** Configure variables. Before running, these file is copied from the secrets
 - **gocd-server.tf:** Configure variables. Before running, these file is copied from the secrets
 - 

# Executing the infrastructure

- Start from scratch:
  `./scripts/runTerraform.sh true`
- Stop non production:
  `./scripts/runTerraform.sh false`
- Destroy all:
  `./scripts/runTerraform.sh destroy`

# Secrets

Secrets in the project are encrypted using https://github.com/AGWA/git-crypt.

Users without permissions will see the contents`input` folder encrypted

## Allowing encrypted files access in new machine

- Generate a gpg key

  `gpg --gen-key`
- You can list the keys in your machine. _*YOUR_KEY*_ is the code listed after the expiration date

  `gpg --list-keys`
- Export new public key (secret key is hosted only in that machine)

  `gpg --armor --export --output ../my-key.gpg $YOUR_KEY`

- A user with the repository already decrypted should add the new key

  `gpg --import ../my-key.gpg`

  `gpg --edit-key $YOUR_KEY` 
  ` trust` [// choose opt 5]  
  ` quit`  

  `git-crypt add-gpg-user $YOUR_KEY`

- User should pull the repository. If encrypted files are still not
  visible unlock them
  `git-crypt unlock`

## If access to all gpg machines is lost

Really low possibility but, if repository is not cloned in any machine
and machines with registered gpg's are also lost the only way to decrypt
the data is using the default g-crypt file which is being be kept in a
safe place.

- clone project
- un-encrypt using

  `git-crypt unlock $FILE_WITH_GIT_CRYPT_DEFAULT_KEY`
- Remove `.git-crypt/keys`
- Generate, add new gpg users following above steps

# Useful fixes

## Import existing resources from aws.

- Use when terraform state is lost.

```
terraform import aws_security_group.internet-access sg-312736287
terraform import aws_instance.production-instance i-031221323
```

From: https://stackoverflow.com/questions/60228784/error-creating-security-group-invalidgroup-duplicate-when-defining-aws-security

- *NOTE:* Use as last resource. Haven't been able to actually make it work.
  Instead, state will be saved as a secret
